package com.variable.webService;

import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Env {
    public static Properties properties;
    static {
        properties = new Properties();
        InputStream inputStream = Configuration.class.getResourceAsStream("/application.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
