package com.variable.webService.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

public class Techie {
    private String name;
    private String id;
    private String skills;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }
}
