package com.variable.webService;

import com.variable.webService.connections.DatabaseConnect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import sun.tools.java.Environment;

import javax.annotation.PreDestroy;
import java.sql.*;
import java.sql.SQLException;
import java.util.logging.Logger;

@SpringBootApplication

public class WebServiceApplication {
	public static final Logger logger = Logger.getLogger(WebServiceApplication.class.getName());
	@Autowired
	DatabaseConnect databaseConnect;

	public static void main(String[] args) {
		ConfigurableApplicationContext context =
				SpringApplication.run(WebServiceApplication.class, args);
	}
}
