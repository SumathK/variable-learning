package com.variable.webService.connections;


import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.variable.webService.Env.properties;
import java.util.logging.Logger;

@Component
public class DatabaseConnect {
    private static final Logger logger = Logger.getLogger(DatabaseConnect.class.getName());
    public static Connection connection;

    public DatabaseConnect(){
        getRemoteConnection();
    }

    public static Connection getRemoteConnection() {

        if(connection != null){
            return connection;
        }
//        BasicDataSource ds = new BasicDataSource();
//        ds.setUsername();
//        ds.setPassword();
//        ds.setUrl();

        if (properties.getProperty("RDS_HOSTNAME") != null) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                String dbName = properties.getProperty("RDS_DB_NAME");
                String userName = properties.getProperty("RDS_USERNAME");
                String password = properties.getProperty("RDS_PASSWORD");
                String hostname = properties.getProperty("RDS_HOSTNAME");
                String port = properties.getProperty("RDS_PORT");
                String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
                connection =  DriverManager.getConnection(jdbcUrl);
                logger.info("Database connection Established");
                return connection;
            } catch (ClassNotFoundException e) {
                logger.warning(e.toString());
            } catch (SQLException e) {
                logger.warning(e.toString());
            }
        }
        System.exit(0);
        return null;
    }
}
