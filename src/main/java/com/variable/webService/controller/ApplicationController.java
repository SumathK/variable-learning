package com.variable.webService.controller;

import com.variable.webService.connections.DatabaseConnect;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

@Configuration
public class ApplicationController {
    private static final Logger logger = Logger.getLogger(ApplicationController.class.getName());

    @PreDestroy
    public void onShutDown() {
        try {

            if (!DatabaseConnect.connection.isClosed()) {
                System.out.println("Closing database connection...");
                DatabaseConnect.connection.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("closing application context..let's do the final resource cleanup");
    }

}
