package com.variable.webService.controller;

import com.variable.webService.connections.DatabaseConnect;
import com.variable.webService.model.Techie;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

@RestController
public class TechieController {
    private static final AtomicLong counter = new AtomicLong();
    private static Logger logger = Logger.getLogger(TechieController.class.getName());

    @GetMapping("/techie")
    public List<Techie> techie(@RequestParam(value="name", defaultValue = "Techie")String name){
        logger.info("GET: Request received for techie");
        List<Techie> techieList = new ArrayList<>();
        try {
            Connection connection = DatabaseConnect.getRemoteConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * from variable.techie;");
            while(rs.next()){
                Techie techie = new Techie();
                techie.setName(rs.getString("name"));
                techie.setSkills(rs.getString("skills"));
                techieList.add(techie);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return techieList;
    }

    @PostMapping("/registerUser")
    @ResponseBody
    public String registerUser(@RequestBody Techie techie) throws SQLException {
        logger.info("POST: registerUser received");
        //query
        String query = "INSERT INTO (name, skills) VALUES (?, ?)";
        Connection conn = DatabaseConnect.getRemoteConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(query);
        preparedStatement.setString(1, techie.getName());
        preparedStatement.setString(2, techie.getSkills());
        return "User registered";
    }

}
